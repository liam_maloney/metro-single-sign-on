
# Metro Area Single Sign On

## Overview
This project contains a helper library which can be used to authenticate 
users on requests to any API on the Metro infrastructure, provided they 
have signed in via community and have injected the Silhouette[MetroJWTEnv]
handler from the helper library.

User sessions are stored in Memcached by Community, and as a stateful approach to the 
Silhouette authenticators is used, every request will be checked against this cache. 
The cache layer is implemented by a Play Memcached plugin.

As every request checks the cache for an active session, it is possible to force a 
user logout by removing the key associated with their authenticator in the cache. 
A particular user may be identified by looking at the 
values in the cache, and searching for that users login credentials (email should 
be visible in the value associated with the key which needs to be deleted).

Using Memcached as a caching layer provides some limited 
protection in the event of Community downtime.  Community is only contacted during 
user login in order to verify the credentials they have provided.  All subsequent 
requests for a User Object will occur via Memcached.  For this reason, authentication 
should still operate while a user has an active session in memcached, even if community is down.

## Configuration (Scala 2.12.5)

1. Add the following to dependencies in build.sbt:
```scala
    val authDeps = Seq(
      "com.mohiva" %% "play-silhouette" % "5.0.0",
      "com.mohiva" %% "play-silhouette-password-bcrypt" % "5.0.0",
      "com.mohiva" %% "play-silhouette-persistence" % "5.0.0",
      "com.mohiva" %% "play-silhouette-crypto-jca" % "5.0.0",
      "com.github.mumoshu" %% "play2-memcached-play26" % "0.9.1",
      "org.foodcloud.metro.authentication" %% "metro-single-sign-on" % "2.1.0",
      cacheApi
    )
```
2. Add resolver for Memcached Plugin in build.sbt:
```scala
    resolvers ++= Seq(
      "Spy Repository" at "http://files.couchbase.com/maven2"
    )
```
3. Specify the required config in a configuration file: `shared_auth_sso.conf`:
```
# This file is shared across all consumers of the SSO library.
# Ensure any changes here are replicated in the client modules

play.modules.enabled += "org.foodcloud.metro.authentication.MetroSSOClientModule"

# Used to encrypt.  Must be a shared application secret across modules to allow
# verification of requests without needing to call back to Community every time.
play.http.secret.key = ${env.logins.shared_session_secret}
login.redirectUrlOnUnauthorised = ${env.logins.unauthorized_redirect_url},
login.redirectUrlOnUnauthenticated = ${env.logins.unauthenticated_redirect_url}
login.metro_web_url = ${env.logins.metro_web_url}
# Field in which the sign in process will look in the request headers for the JWT
play.http.session.cookieName="METRO_SESSION"
login.authenticator.fieldName = "METRO_JWT"
login.authenticator.crypter.key = ${play.http.secret.key}
login.authenticator.sharedSecret = ${play.http.secret.key}
login.signout.url = ${env.logins.signout.url}

# Disabled default error handlers for silhouette as we are using custom redirects
play.modules.disabled += "com.mohiva.play.silhouette.api.actions.SecuredErrorHandlerModule"
play.modules.disabled += "com.mohiva.play.silhouette.api.actions.UnsecuredErrorHandlerModule"

# Memcached settings for user authenticators
memcached.enabled = true
memcached.host = ${env.memcached.host}
memcached.username = ${env.memcached.username}
memcached.password = ${env.memcached.password}

# Disabled default caches and bind new ones as we are now using Memcached
play.modules.cache.defaultCache = default
play.modules.cache.bindCaches = ["db-cache", "user-cache", "session-cache"]
play.modules.enabled += "com.github.mumoshu.play2.memcached.MemcachedModule"


```
  
## Sample Usage/Securing Endpoints

Under the hood, the authentication mechanism uses JWTs to authenticate a user, by looking 
for either a JWT in the play session, or in a header.  As JWTs support the 
idea of embedding custom arbitrarily nested data via the Scopes 
field, authorization is also possible in addition to authentication.

##### Endpoint Guards

```scala

import play.api.mvc._
import javax.inject._
import com.mohiva.play.silhouette.api.Silhouette
import scala.concurrent.{ExecutionContext, Future}
import org.foodcloud.metro.authentication.model.MetroUser
// Links the JWTAuthenticator and the IdentityService
import org.foodcloud.metro.authentication.environment.MetroJWTEnv

@Singleton
class HomeController @Inject()(silhouette: Silhouette[MetroJWTEnv]) extends Controller {

    /** With the MetroJWTEnv injected, we can handle authentication + authorization **/
    def index() = silhouette.SecuredAction.async { implicit request =>
      // The model of the authenticated user is available as per Community Model, pulled from Memcached
      val user: MetroUser = request.identity
      Future.successful(Ok)
    }
  
    /** An unauthenticated endpoint **/
    def ep1() = silhouette.UnsecuredAction.async { 
      Future.successful(Ok) 
    }
    
    /** An authenticated endpoint **/
    def ep2() = silhouette.UserAwareAction.async { implicit request => 
      val maybeLoggedIn: Option[MetroUser] = request.identity  
      Future.successful(Ok) 
    }
}
    
```

## Easy Local Memcached Setup
If docker is installed, simply run `docker run -p 11211:11211 -d memcached` to start a Memcached instance on port 11211

## Interacting with Memcached (And removing a user session)
 * connecting: 
 `telnet localhost 11211`
 * view slab items (Area where key values are stored): 
 `stats items`
```
 STAT items:17:number 1
 STAT items:17:number_hot 0
 STAT items:17:number_warm 0
 STAT items:17:number_cold 1
 STAT items:17:age_hot 0
 STAT items:17:age_warm 0
 STAT items:17:age 209
 STAT items:17:evicted 0
 STAT items:17:evicted_nonzero 0
 STAT items:17:evicted_time 0
 STAT items:17:outofmemory 0
 STAT items:17:tailrepairs 0
 STAT items:17:reclaimed 0
 STAT items:17:expired_unfetched 0
 STAT items:17:evicted_unfetched 0
 STAT items:17:evicted_active 0
 STAT items:17:crawler_reclaimed 0
 STAT items:17:crawler_items_checked 0
 STAT items:17:lrutail_reflocked 0
 STAT items:17:moves_to_cold 1
 STAT items:17:moves_to_warm 0
 STAT items:17:moves_within_lru 0
 STAT items:17:direct_reclaims 0
 STAT items:17:hits_to_hot 1
 STAT items:17:hits_to_warm 0
 STAT items:17:hits_to_cold 0
 STAT items:17:hits_to_temp 0
 END
```
 * dump keys for a slab: `stats cachedump 17 1`, where 17 is the slab ID, and 1 is the amount to dump
```
 ITEM default546b1bdd52df28d1839bb10bc3e64de4ddcecee788621e8b0bf2bd1cb2c6afa394562317599529cd2d8f3c490b8a53801f7dbed543c5edca0ce8f4eb3979cfc5 [2973 b; 0 s]
```
 * view value of key `get default54...8f4eb3979cfc5`, 
    Note that a lot of the output is trimmed, but the users email will be greppable in the output.

```
get default546b1...bda0ce8f4eb3979cfc5
VALUE ...liam@foodcloud.ie...
```

* Remove the session, causing a user logout:  
`delete default54...8f4eb3979cfc5`
