name := "metro-single-sign-on"

organization := "org.foodcloud.metro.authentication"

version := "2.1.0"

scalaVersion := "2.12.5"

publishTo := Some("releases" at "https://mymavenrepo.com/repo/ou8dJuHGO5zQVKWnbV4k/")

// required to resolve `spymemcached`
resolvers += "Spy Repository" at "http://files.couchbase.com/maven2"

libraryDependencies ++= Seq(
  "com.mohiva" %% "play-silhouette" % "5.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "5.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "5.0.0",
  "com.mohiva" %% "play-silhouette-testkit" % "5.0.0",
  "com.iheart" %% "ficus" % "1.4.3",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "5.0.0",
  "com.github.mumoshu" %% "play2-memcached-play26" % "0.9.1",
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "net.codingwell" %% "scala-guice" % "4.1.0"
)