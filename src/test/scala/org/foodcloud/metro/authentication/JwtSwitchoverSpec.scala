package org.foodcloud.metro.authentication

import org.scalatest.FlatSpec
import org.foodcloud.metro.authentication.environment._

class JwtSwitchoverSpec extends FlatSpec {

  import JWTAuthenticatorFromHeaderOrSession.jwtFromSessionToHeader

  info(raw"""
   |
   |Tests to ensure correct extraction of JWT token from session or header.
   |Both methods of authentication are supported (header + session) by promoting
   |all tokens which exist in the session to live in the header during request handling
   |
   |""".stripMargin
  )

  private val mockJwtKey = "METRO_AREA_JWT"
  private val mockJwtValue = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9." +
    "eyJzdWIiOiIxLThqdmUrN2dTbDZyWUhTaDFxTGU5Z0lOV0hNQWEwODBMQ1ZaQUxZT2lnbktqZnZjZzN5VGFWcnB1SXYxaUZiNTJVc" +
    "2pqVFZ2dlNhS2NKNWM4czhBR1RzNFdrUnAwakZ4RE85ZmREQzJpIiwicm9sZSI6IkJvc3NBZG1pbiIsImlzcyI6InBsYXktc2lsaG91" +
    "ZXR0ZSIsImV4cCI6MTUxNTY3MTY3MiwiaWF0IjoxNTEzMDc5NjcyLCJ1c2VyIjp7ImVtYWlsIjoibGlhbUBmb29kY2xvdWQuaWUiLC" +
    "Jyb2xlIjoiQm9zc0FkbWluIiwib3JnSWQiOjEyOTMsImxvY2FsZSI6ImVuIiwibGFzdE5hbWUiOiJNYWxvbmV5IiwidmVyc2lvbiI6" +
    "MCwiZmlyc3ROYW1lIjoiTGlhbSIsInByb2ZpbGUiOiJMaWFtIiwic3RhdHVzIjoiQWN0aXZlIiwibG9naW5JbmZvIjp7InByb3ZpZGVy" +
    "SUQiOiJjcmVkZW50aWFscyIsInByb3ZpZGVyS2V5IjoibGlhbUBmb29kY2xvdWQuaWUifSwibm90ZXMiOiJGb29kQ2xvdWQiLCJ1c2" +
    "VySWQiOiIxZjRlZWRjMC0yNjVlLTQxZjgtOWJkMS0yMThjMjgyZmNiMGMiLCJyZWdpb25JZCI6MSwiY3JlYXRlZCI6IjIwMTctMDg" +
    "tMTBUMTU6MjI6MjYuMzQ3In0sImp0aSI6IjY5OTVmMDBlY2ZkY2FjNDE2OGY5NGE2NjA0NzNmMjczN2RhNDVjZDg4MGY0OGZhZDY2N" +
    "TM4M2ZkNTQ3ZTgzNTUwZTk0NmNhYWI2NGE5ODRlNzExZDYwY2VlNzY5MDc3ZWMwOGU3MjJhZjZiMDU2ZGE3Y2RmMzAzNTE2OTczMGY4In0." +
    "aGyBJeJqB7aWL_nqeT0N3NwW1WhgIvKiGexjWASzdPI"

  "The Custom overridden JWT extractor" should "extract the jwt from the session object to the header" in {

    val result = jwtFromSessionToHeader[Any](
      headers = Map.empty,
      session = Map(mockJwtKey -> mockJwtValue),
      authTokenFieldName = mockJwtKey
    )

    assert(result(mockJwtKey) == mockJwtValue)
  }

  it should "be able to identify the correct key in the session object to transfer based on the field name" in {

    val session = Map(
      "shouldIgnoreThisKey1" -> "AndThisValue1",
      mockJwtKey -> mockJwtValue,
      "shouldIgnoreThisKey2" -> "AndThisValue2"
    )

    val result = jwtFromSessionToHeader[Any](Map.empty, session, authTokenFieldName = mockJwtKey)

    assert(result.isDefinedAt(mockJwtKey) && result(mockJwtKey) == mockJwtValue && result.size == 1)
  }

  it should "leave existing headers unaltered in the original request" in {

    val session = Map(
      "shouldIgnoreThisKey1" -> "AndThisValue1",
      mockJwtKey -> mockJwtValue,
      "shouldIgnoreThisKey2" -> "AndThisValue2"
    )

    val existingHeaders = Map("existing" -> "header")

    val result = jwtFromSessionToHeader[Any](existingHeaders, session, authTokenFieldName = mockJwtKey)

    assert(
      result.isDefinedAt(mockJwtKey) &&
      result.isDefinedAt("existing") &&
      result(mockJwtKey) == mockJwtValue &&
      result("existing") == "header" &&
      result.size == 2
    )
  }

  it should "use the token in the header if one exists in both the header and the session" in {

    val shouldUseTheHeaderToken = "xyz"

    val session = Map(
      "shouldIgnoreThisKey1" -> "AndThisValue1",
      mockJwtKey -> mockJwtValue,
      "shouldIgnoreThisKey2" -> "AndThisValue2"
    )

    val existingHeaders = Map(mockJwtKey -> shouldUseTheHeaderToken)

    val result = jwtFromSessionToHeader[Any](existingHeaders, session, authTokenFieldName = mockJwtKey)

    assert(
        result.isDefinedAt(mockJwtKey) &&
        result(mockJwtKey) == shouldUseTheHeaderToken &&
        result.size == 1
    )

  }

}
