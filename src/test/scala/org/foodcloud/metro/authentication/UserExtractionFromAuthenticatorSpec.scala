package org.foodcloud.metro.authentication

import com.mohiva.play.silhouette.api.repositories.AuthenticatorRepository
import com.mohiva.play.silhouette.api.util.Clock
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import org.foodcloud.metro.authentication.environment.LoginInfoWithAuthenticatorID
import org.foodcloud.metro.authentication.model.MetroUser
import org.foodcloud.metro.authentication.utils.JsonConverters
import org.scalatest.FlatSpec
import scala.concurrent.duration._
import play.api.libs.json.JsObject
import scala.concurrent.{Await, Future}

/**
  * By embedding the user profile associated with the authenticator in its
  * [[com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator.customClaims customClaims]]
  * object, we can avoid needing to return to Community each and every time we want to validate a
  * user/access their properties in some way.
  *
  * To achieve this, we need to rewire the implementation of the User service for consumers of the
  * authentication library to reach into the cache and deserialize the user profile from the
  * JWTAuthenticators customClaims object (if set).
  *
  */

class UserExtractionFromAuthenticatorSpec extends FlatSpec {

  import org.foodcloud.metro.authentication.environment.UserServiceFromAuthenticator._
  import com.mohiva.play.silhouette.api.Authenticator.Implicits._

  val mockUser = MetroUser(
    "test@test.test",
    Vector.empty
  )

  val now = Clock().now

  val authenticatorID = "1"
  val JWTAuthenticatorWithUserEmbedded = JWTAuthenticator(
    id = authenticatorID,
    loginInfo = LoginInfoWithAuthenticatorID("PID", "PK", "1"),
    lastUsedDateTime = now,
    expirationDateTime = now + 2.seconds,
    idleTimeout = Some(10.seconds),
    customClaims = Some(JsObject(Map(
      "user" -> JsonConverters.userFormat.writes(mockUser),
    )))
  )

  class MockedCache extends AuthenticatorRepository[JWTAuthenticator] {
    override def find(id: String) = {
      Future.successful(
        Map(
          "1" -> JWTAuthenticatorWithUserEmbedded
        ).get(id)
      )
    }
    override def add(authenticator: JWTAuthenticator) = null
    override def update(authenticator: JWTAuthenticator) = null
    override def remove(id: String) = null
  }

  val mockedCache = new MockedCache

  "The IdentityService which pulls from the authenticator" should "find a User in the authenticator" in {
    Await.result(searchCacheForAuthenticator("1", mockedCache), 3.seconds) match {
      case Some(user) => user == mockUser
      case None => assert(false)
    }
  }

  it should "not be able to find any user" in {
    Await.result(searchCacheForAuthenticator("badID", mockedCache), 3.seconds) match {
      case Some(user) => assert(false)
      case None => assert(true)
    }
  }
}


