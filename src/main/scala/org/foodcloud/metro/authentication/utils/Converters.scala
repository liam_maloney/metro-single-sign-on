package org.foodcloud.metro.authentication.utils

import java.time.{LocalDate, LocalDateTime, LocalTime}
import java.time.format.DateTimeFormatter
import java.util.Locale

import org.foodcloud.metro.authentication.model.{MetroUser, UserStatus}
import org.foodcloud.metro.authentication.model.UserStatus.UserStatus
import play.api.libs.json._

object Converters {

  implicit object localDateFormat extends Format[LocalDate] {
    def reads(json: JsValue) = JsSuccess(LocalDate.parse(json.as[String]))
    def writes(date: LocalDate) = JsString(date.toString)
  }

  implicit object localTimeFormat extends Format[LocalTime] {
    def reads(json: JsValue) = JsSuccess(LocalTime.parse(json.as[String]))
    def writes(time: LocalTime) = JsString(time.toString)
  }

  implicit object localDateTimeFormat extends Format[LocalDateTime] {
    def reads(json: JsValue) = {
      val odt = DateTimeFormatter.ISO_DATE_TIME.parse(json.as[String])
      JsSuccess(LocalDateTime.from(odt))
    }
    def writes(datetime: LocalDateTime) = JsString(datetime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
  }

  implicit object localeFormat extends Format[Locale] {
    def reads(json: JsValue) = JsSuccess(Locale.forLanguageTag(json.as[String]))
    def writes(value: Locale) = JsString(value.toString)
  }

  implicit object userStatusFormat extends Format[UserStatus] {
    def reads(json: JsValue) = JsSuccess(UserStatus.withName(json.as[String]))
    def writes(status: UserStatus) =  JsString(status.toString)
  }

}

object JsonConverters {

  implicit object userStatusFormat extends Format[UserStatus] {
    def reads(json: JsValue) = JsSuccess(UserStatus.withName(json.as[String]))
    def writes(status: UserStatus) = JsString(status.toString)
  }

  implicit object localeFormat extends Format[Locale] {
    def reads(json: JsValue) = JsSuccess(new Locale(json.as[String]))
    def writes(locale: Locale) = JsString(locale.toString)
  }

  implicit val userFormat = Json.format[MetroUser]
}
