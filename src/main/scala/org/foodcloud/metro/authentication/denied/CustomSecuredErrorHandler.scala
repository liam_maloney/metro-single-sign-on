package org.foodcloud.metro.authentication.denied

import javax.inject.Inject
import com.mohiva.play.silhouette.api.actions.SecuredErrorHandler
import language._
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.RequestHeader
import play.api.mvc.Results._
import scala.concurrent.Future

class CustomSecuredErrorHandler @Inject()(val messagesApi: MessagesApi,
                                          config: Configuration) extends SecuredErrorHandler with I18nSupport {

  private val postSignInRedirect = "postSignInRedirect"

  override def onNotAuthenticated(implicit request: RequestHeader) =
    Future.successful(
      Redirect(config getString "login.redirectUrlOnUnauthenticated" get).withSession(
        postSignInRedirect -> (request.headers("host") + request.path)
      )
    )

  override def onNotAuthorized(implicit request: RequestHeader) =
    Future successful Redirect(config getString "login.redirectUrlOnUnauthorised" get).withSession(
      postSignInRedirect -> (request.headers("host") + request.path)
    )

}