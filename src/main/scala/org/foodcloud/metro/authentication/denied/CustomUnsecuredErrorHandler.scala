
package org.foodcloud.metro.authentication.denied

import language._
import play.api.mvc.Results._
import play.api.Configuration
import scala.concurrent.Future
import com.google.inject.Inject
import play.api.mvc.RequestHeader
import play.api.i18n.{I18nSupport, MessagesApi}
import com.mohiva.play.silhouette.api.actions.UnsecuredErrorHandler

class CustomUnsecuredErrorHandler @Inject() (val messagesApi: MessagesApi,
                                             config: Configuration) extends UnsecuredErrorHandler with I18nSupport {

  override def onNotAuthorized(implicit request: RequestHeader) =
    Future successful Redirect(config getString "login.redirectUrlOnUnauthenticated" get)

}