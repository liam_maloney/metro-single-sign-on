package org.foodcloud.metro.authentication

import com.google.inject.{AbstractModule, Provides}
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.actions.{SecuredErrorHandler, UnsecuredErrorHandler}
import com.mohiva.play.silhouette.api.repositories.AuthenticatorRepository
import services._
import com.mohiva.play.silhouette.api.util.{CacheLayer, _}
import com.mohiva.play.silhouette.crypto.{JcaCrypter, JcaCrypterSettings}
import com.mohiva.play.silhouette.impl.authenticators._
import com.mohiva.play.silhouette.impl.util._
import com.mohiva.play.silhouette.persistence.repositories.CacheAuthenticatorRepository
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.ceedubs.ficus.readers.EnumerationReader._
import net.codingwell.scalaguice.ScalaModule
import play.api.Configuration
import play.api.libs.ws.WSClient
import com.mohiva.play.silhouette.api.crypto.{Crypter, CrypterAuthenticatorEncoder}
import org.foodcloud.metro.authentication.denied.{CustomSecuredErrorHandler, CustomUnsecuredErrorHandler}
import org.foodcloud.metro.authentication.environment._
import play.api.libs.crypto.{CookieSigner, CookieSignerProvider}
import scala.concurrent.ExecutionContext.Implicits.global

class MetroSSOClientModule extends AbstractModule with ScalaModule {

  def configure() {

    bind[Silhouette[MetroJWTEnv]].to[SilhouetteProvider[MetroJWTEnv]]
    bind[UnsecuredErrorHandler].to[CustomUnsecuredErrorHandler]
    bind[SecuredErrorHandler].to[CustomSecuredErrorHandler]
    bind[CacheLayer].to[PlayCacheLayer]
    bind[Clock].toInstance(Clock())
    bind[IDGenerator].toInstance(new SecureRandomIDGenerator(64))
    bind[CookieSigner].toProvider[CookieSignerProvider]
  }


  @Provides
  def provideHTTPLayer(client: WSClient): HTTPLayer =

    new PlayHTTPLayer(client)


  @Provides
  def provideEnvironment(authRepo: AuthenticatorRepository[JWTAuthenticator],
                         authenticatorService: AuthenticatorService[JWTAuthenticator],
                         eventBus: EventBus): Environment[MetroJWTEnv] =

    Environment[MetroJWTEnv](
      UserServiceFromAuthenticator(authRepo),
      authenticatorService,
      Seq(),
      eventBus
    )


  @Provides
  def provideAuthenticatorCrypter(configuration: Configuration): Crypter =

    new JcaCrypter(configuration.underlying.as[JcaCrypterSettings]("login.authenticator.crypter"))


  @Provides
  def provideAuthenticatorService(crypter: Crypter,
                                  idGenerator: IDGenerator,
                                  configuration: Configuration,
                                  loginBackingStore: AuthenticatorRepository[JWTAuthenticator],
                                  clock: Clock): AuthenticatorService[JWTAuthenticator] =

    JWTAuthenticatorFromHeaderOrSession (
      clock = clock,
      authenticatorEncoder = new CrypterAuthenticatorEncoder(crypter),
      idGenerator = idGenerator,
      repository = Some(loginBackingStore),
      settings = configuration.underlying.as[JWTAuthenticatorSettings]("login.authenticator")
    )

  @Provides
  def jwtBackingStore(cacheImpl: CacheLayer): AuthenticatorRepository[JWTAuthenticator] = {
    new CacheAuthenticatorRepository(cacheImpl)
  }

}