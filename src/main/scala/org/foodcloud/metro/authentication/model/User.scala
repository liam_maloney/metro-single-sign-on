package org.foodcloud.metro.authentication.model

import com.mohiva.play.silhouette.api.Identity

object UserStatus extends Enumeration {
  type UserStatus = Value
  val New = Value("New")
  val Active = Value("Active")
  val Confirmed = Value("Confirmed")
  val Suspended = Value("Suspended")
  val Rejected = Value("Rejected")
}

/**
  *
  * @param email The natural id for a user in the metro system.
  * @param visibilityStrings Flat Representation of parent pointers in the Org Hierarchies
  *                          of which the user is a member.
  *
  */

case class MetroUser(email: String,
                     visibilityStrings: Vector[String]) extends Identity
