package org.foodcloud.metro.authentication.environment

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import org.foodcloud.metro.authentication.model.MetroUser

trait MetroJWTEnv extends Env {
  type I = MetroUser
  type A = JWTAuthenticator
}
