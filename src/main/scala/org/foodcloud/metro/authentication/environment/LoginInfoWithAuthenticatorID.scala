package org.foodcloud.metro.authentication.environment

import com.mohiva.play.silhouette.api.LoginInfo

@SerialVersionUID(114L)
class LoginInfoWithAuthenticatorID(override val providerID: String,
                                   override val providerKey: String,
                                   val authenticatorKey: String
                                  ) extends LoginInfo(providerID, providerKey) with Serializable


object LoginInfoWithAuthenticatorID {

  def unapply(liwa: LoginInfoWithAuthenticatorID): Option[(String, String, String)] =
    Some((liwa.providerID, liwa.providerKey, liwa.authenticatorKey))

  def apply(providerID: String, providerKey: String, authenticatorKey: String): LoginInfoWithAuthenticatorID =
    new LoginInfoWithAuthenticatorID(providerID, providerKey, authenticatorKey)
}