package org.foodcloud.metro.authentication.environment

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.repositories.AuthenticatorRepository
import com.mohiva.play.silhouette.api.services.IdentityService
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import org.foodcloud.metro.authentication.model.MetroUser
import org.foodcloud.metro.authentication.utils.JsonConverters

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import UserServiceFromAuthenticator._

case class UserServiceFromAuthenticator(authenticatorCache: AuthenticatorRepository[JWTAuthenticator])
  extends IdentityService[MetroUser] {

  override def retrieve(loginInfo: LoginInfo): Future[Option[MetroUser]] = loginInfo match {
    case LoginInfoWithAuthenticatorID(_, _, authenticatorKey) =>
      searchCacheForAuthenticator(authenticatorKey, authenticatorCache)
    case _ =>
      Future.successful(None)
  }

}

object UserServiceFromAuthenticator {

  def searchCacheForAuthenticator(authenticatorId: String, authCache: AuthenticatorRepository[JWTAuthenticator]) =
    authCache.find(authenticatorId) map {
      case Some(authenticator) if authenticator.customClaims.isDefined =>
        JsonConverters.userFormat.reads((authenticator.customClaims.get \ "user").get).asOpt
      case _ =>
        None
    }

}