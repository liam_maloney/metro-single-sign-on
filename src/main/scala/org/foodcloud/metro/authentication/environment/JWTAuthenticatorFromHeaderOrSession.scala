package org.foodcloud.metro.authentication.environment

import com.mohiva.play.silhouette.api.crypto.AuthenticatorEncoder
import com.mohiva.play.silhouette.api.repositories.AuthenticatorRepository
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.impl.authenticators._
import scala.concurrent.ExecutionContext.Implicits._
import play.api.mvc._

/**
  * Silhouette as of v4.0 does not support JWT token extraction from a Play Session.
  * For a list of the supported areas from which a token may be extracted, see:
  * [[com.mohiva.play.silhouette.api.util.RequestExtractor RequestExtractor]]
  *
  * As a Session embedded token appears to be the most convenient way to implemented SSO,
  * this class extends the [[JWTAuthenticator JWTAuthenticator]] to intercept the a call
  * to retrieve the token while a request is handled.
  *
  * The reimplementation should transfer JWTTokens from the session to the header, so that
  * the Silhouette infrastructure can handle it from that point forward as if the token was
  * coming from a header to begin with.
  */

case class JWTAuthenticatorFromHeaderOrSession(settings: JWTAuthenticatorSettings,
                                               repository: Option[AuthenticatorRepository[JWTAuthenticator]],
                                               authenticatorEncoder: AuthenticatorEncoder,
                                               idGenerator: IDGenerator,
                                               clock: Clock)
  extends JWTAuthenticatorService(settings, repository, authenticatorEncoder, idGenerator, clock) {

  import JWTAuthenticatorFromHeaderOrSession._

  override def retrieve[B](implicit request: ExtractableRequest[B]) =
    super.retrieve(new ExtractableRequest(Request[B](request.copy(headers = new Headers(
      jwtFromSessionToHeader(request.headers.toSimpleMap, request.session.data, settings.fieldName).toSeq
    )), request.body)))

}

object JWTAuthenticatorFromHeaderOrSession {

  def jwtFromSessionToHeader[B](headers: Map[String, String], session: Map[String, String], authTokenFieldName: String) =
    headers get authTokenFieldName map { _ => headers } getOrElse {
      session get authTokenFieldName map {
        jwtFromSession => headers + (authTokenFieldName -> jwtFromSession)
      } getOrElse headers
    }

}